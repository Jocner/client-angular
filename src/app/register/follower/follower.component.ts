import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-follower',
  templateUrl: './follower.component.html',
  styleUrls: ['./follower.component.css']
})
export class FollowerComponent{
  @Input() followerForm!: FormGroup;
  @Input() nuevoFollowerGroup!: FormGroup;
  @Input() totalAsistentes!: number;
  @Input() totalGeneral!: number;
  @Input() totalVehiculos!: number;
  @Input() EventoTipo!:string;
  disabled: boolean = true;


  constructor() { }

  validarTipoEvento() {
    if (this.EventoTipo == "Grupo Pequeño") {
      return false;
    } else {
      return true;
    }
  }

}
