import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { RegisterRequest } from 'src/app/interfaces/register-request';
import { RegisterService } from 'src/app/service/register.service';

import Swal from "sweetalert2";

@Component({
  selector: 'app-resgister-home',
  templateUrl: './resgister-home.component.html',
  styleUrls: ['./resgister-home.component.css']
})
export class ResgisterHomeComponent {
  dataEventForm!: FormGroup;
  nuevoEventGroup!: FormGroup;
  asistenciaForm!: FormGroup;
  nuevoAsistenciaGroup!: FormGroup;
  followerForm!: FormGroup;
  nuevoFollowerGroup!: FormGroup;
  responsibleForm!: FormGroup;
  nuevoResponsibleGroup!: FormGroup;

  disabled: boolean = false;
  totalAsistentes: number = 0;
  totalGeneral: number = 0;
  totalVehiculos: number = 0;

  @Input() recibirHorario!: string;
  cambiarHora:boolean = false;
  @Input() recibirDescripcion!: string;
  EventoTipo!: string;
  CampusTipo!: string;


  constructor(private formBuilder: FormBuilder, private registerService: RegisterService) {

    this.dataEventForm = this.formBuilder.group(
      {
        tipoEvento: ['', Validators.required],
        fecha: ['', Validators.required],
        hora: ['', Validators.required],
        campus: [''],
        descripcion: [''],
      }
    );
    this.nuevoEventGroup = this.formBuilder.group({
      event: this.formBuilder.array(
        [], Validators.required
      )
    });

    this.responsibleForm = this.formBuilder.group(
      {
        lideresVoluntarios: [''],
        directores: [''],
        administradores: [''],
        predicador: [''],
        tituloMensaje: [''],
      }
    );
    this.nuevoResponsibleGroup = this.formBuilder.group({
      responsible: this.formBuilder.array(
        [], Validators.required
      )
    });

    this.asistenciaForm = this.formBuilder.group(
      {
        asistenciaAuditorio: [''],
        asistenciaKids: [''],
        asistenciaTweens: [''],
        asistenciaTeens: [''],
        voluntariosServicios: [''],
        voluntariosTecnica: [''],
        voluntariosKids: [''],
        voluntariosTweens: [''],
        voluntariosTeens: [''],
        voluntariosInfo: [''],
        voluntariosWorship: [''],
        voluntariosCocina: [''],
        voluntariosRedesSociales: [''],
        autos: [''],
        motos: [''],
        bicicletas: [''],
      }
    );
    this.nuevoAsistenciaGroup = this.formBuilder.group({
      asistencia: this.formBuilder.array(
        [], Validators.required
      )
    });
    this.followerForm = this.formBuilder.group(
      {
        personasAceptanJesusPresencial: ['', [Validators.required, Validators.min(0)]],
        personasAceptanJesusOnline: ['', [Validators.required, Validators.min(0)]],
      }
    );
    this.nuevoFollowerGroup = this.formBuilder.group({
      follower: this.formBuilder.array(
        [], Validators.required
      )
    });

   }

  recibirHora(horario:string){
    this.recibirHorario = horario;
    this.cambiarHora = true;
  }

  recibirDescrip(descripcion: string) {
    this.dataEventForm.value.descripcion = descripcion;

  }


  calculosTotalesAsistencias(): void{
    if (this.asistenciaForm.valid) {
      const reg = this.asistenciaForm.value

      this.totalAsistentes = reg.asistenciaAuditorio + reg.asistenciaKids +
        reg.asistenciaKids + reg.asistenciaTweens + reg.asistenciaTeens;


      this.totalGeneral = reg.asistenciaAuditorio + reg.asistenciaKids +
        reg.asistenciaKids + reg.asistenciaTweens + reg.asistenciaTeens + reg.voluntariosServicios +
        reg.voluntariosTecnica + reg.voluntariosKids + reg.voluntariosTweens + reg.voluntariosTeens +
        reg.voluntariosInfo + reg.voluntariosWorship + reg.voluntariosCocina + reg.voluntariosRedesSociales;

      this.totalVehiculos = reg.autos + reg.motos + reg.bicicletas;
    } else {
      this.totalAsistentes = 0;
      this.totalGeneral = 0;
      this.totalVehiculos = 0;
    }

  }

  recibirTipoEvento(tipoEvento:string){
    this.EventoTipo = tipoEvento;
  }

  recibirTipoCampus(campus:string){
    this.CampusTipo = campus;
    this.validarCampus();
  }

  validarTipoEvento(){

    if (this.EventoTipo == "Oración") {
      this.responsibleForm.value.predicador = "";
      this.responsibleForm.value.tituloMensaje = "";

    }
    if (this.EventoTipo == "Presentación de Bebes") {
      this.responsibleForm.value.tituloMensaje = "";

    }
    if (this.EventoTipo == "Bautismos") {
      this.responsibleForm.value.tituloMensaje = "";

    }
    if(this.EventoTipo == "Grupo Pequeño") {
      this.responsibleForm.value.predicador = "";
      this.responsibleForm.value.tituloMensaje = "";
      this.responsibleForm.value.directores = "";
      this.responsibleForm.value.administradores = "";
      this.responsibleForm.value.lideresVoluntarios="";

      this.dataEventForm.value.campus = "Virtual";

      this.followerForm.value.personasAceptanJesusPresencial = 0;

      this.asistenciaForm.value.asistenciaAuditorio = 0;
      this.asistenciaForm.value.asistenciaKids = 0;
      this.asistenciaForm.value.asistenciaTweens = 0;
      this.asistenciaForm.value.asistenciaTeens = 0;
      this.asistenciaForm.value.voluntariosServicios = 0;
      this.asistenciaForm.value.voluntariosTecnica = 0;
      this.asistenciaForm.value.voluntariosKids = 0;
      this.asistenciaForm.value.voluntariosTweens = 0;
      this.asistenciaForm.value.voluntariosTeens = 0;
      this.asistenciaForm.value.voluntariosInfo = 0;
      this.asistenciaForm.value.voluntariosWorship = 0;
      this.asistenciaForm.value.voluntariosCocina = 0;
      this.asistenciaForm.value.voluntariosRedesSociales = 0;
      this.asistenciaForm.value.autos = 0;
      this.asistenciaForm.value.motos = 0;
      this.asistenciaForm.value.bicicletas = 0;

      return false;
    } else {
      return true;
    }
  }

  validarCampus(){
    if (this.CampusTipo != "Santiago"){
      this.responsibleForm.value.directores = "";
    }
  }

  verificarHora(){
    if(this.cambiarHora){
      this.dataEventForm.value.hora = this.recibirHorario;
    }
  }

  formatearFecha(fecha:string): string{
    let formatted_date = "";
    let date: Date = new Date(fecha);

      if ((date.getMonth() + 1) > 9){
        if (date.getDate() < 10){
          formatted_date = "0"+ date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        } else {
          formatted_date = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        }
      }else {
          if (date.getDate() < 10) {
            formatted_date = "0" + date.getDate() + "-" + "0" + (date.getMonth() + 1) + "-" + date.getFullYear();
          } else {
            formatted_date = date.getDate() + "-" + "0" + (date.getMonth() + 1) + "-" + date.getFullYear();
          }
        }
    return formatted_date;
  }

  validarForm(){
    if (this.dataEventForm.valid && this.followerForm.valid && this.responsibleForm.valid && this.asistenciaForm.valid){
      return true;
    }
    else{
      return false;
    }
  }

  validarFormAndTipoEvento(){
    if (this.EventoTipo == "Grupo Pequeño" && this.dataEventForm.valid) {
      return true;
    } else {
      return false;
    }
  }

  sendForm() {
    this.calculosTotalesAsistencias();
    this.verificarHora();
    this.validarCampus();
    let fechaFormateada = this.formatearFecha(this.dataEventForm.value.fecha);


      const reg = this.dataEventForm.value;
      const asist = this.asistenciaForm.value;
      const foll = this.followerForm.value;
      const res = this.responsibleForm.value;
      this.disabled = true;

    if (this.validarFormAndTipoEvento() || this.validarForm()) {
      let formulario: RegisterRequest = {
        "tipoEvento": reg.tipoEvento,
        "fecha": fechaFormateada,
        "hora": reg.hora,
        "campus": reg.campus,
        "descripcion": reg.descripcion,
        "lideresVoluntarios": res.lideresVoluntarios,
        "directores": res.directores,
        "administradores": res.administradores,
        "predicador": res.predicador,
        "tituloMensaje": res.tituloMensaje,
        "asistenciaAuditorio": asist.asistenciaAuditorio,
        "asistenciaKids": asist.asistenciaKids,
        "asistenciaTweens": asist.asistenciaTweens,
        "asistenciaTeens": asist.asistenciaTeens,
        "voluntariosServicios": asist.voluntariosServicios,
        "voluntariosTecnica": asist.voluntariosTecnica,
        "voluntariosKids": asist.voluntariosKids,
        "voluntariosTweens": asist.voluntariosTweens,
        "voluntariosTeens": asist.voluntariosTeens,
        "voluntariosInfo": asist.voluntariosInfo,
        "voluntariosWorship": asist.voluntariosWorship,
        "voluntariosCocina": asist.voluntariosCocina,
        "voluntariosRedesSociales": asist.voluntariosRedesSociales,
        "autos": asist.autos,
        "motos": asist.motos,
        "bicicletas": asist.bicicletas,
        "personasAceptanJesusPresencial": foll.personasAceptanJesusPresencial,
        "personasAceptanJesusOnline": foll.personasAceptanJesusOnline,
        "totalAsistentes": this.totalAsistentes,
        "totalGeneral": this.totalGeneral,
        "totalVehiculos": this.totalVehiculos,
      }

      try {
        this.registerService.postRegister(formulario).subscribe(_response => {
          Swal.fire({
            icon: "success",
            title: "Éxito",
            text: "Envió registro con Éxito",
          });
        });

        this.dataEventForm.reset();
        this.asistenciaForm.reset();
        this.followerForm.reset();
        this.responsibleForm.reset();
      } catch (err) {
        console.log("Error: " + err);
        Swal.fire({
          icon: "error",
          title: "No se pudo enviar",
          text: "El registro no se pudo enviar, intente nuevamente"
        });
      }
    } else  {
      this.dataEventForm.markAllAsTouched();
      this.asistenciaForm.markAllAsTouched();
      this.followerForm.markAllAsTouched();
      this.responsibleForm.markAllAsTouched();

      Swal.fire({
        icon: "error",
        title: "No se pudo enviar",
        text: "Complete todos los elementos"
      });
    }



  }
}
