import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AdministradorResponse, DirectorResponse, LiderResponse } from 'src/app/interfaces/listas-response';
import { ListasService } from 'src/app/service/listas.service';

@Component({
  selector: 'app-responsible',
  templateUrl: './responsible.component.html',
  styleUrls: ['./responsible.component.css']
})
export class ResponsibleComponent implements OnInit {

  @Input() responsibleForm!: FormGroup;
  @Input() nuevoResponsibleGroup!: FormGroup;
  @Input() EventoTipo!:string;
  @Input() CampusTipo!:string;

  administradores:AdministradorResponse[] = [];
  lideresVoluntario: LiderResponse[] = [];
  directores: DirectorResponse[] = [];

  constructor(private listasService: ListasService) { }

  ngOnInit(): void {
    this.listasService.getAdministrador().subscribe(response => {
      this.administradores = response;
      console.log(response);
    });
    this.listasService.getLider().subscribe(response => {
      this.lideresVoluntario = response;
      console.log(response);
    });
    this.listasService.getDirector().subscribe(response => {
      this.directores = response;
      console.log(response);
    })
  }


  validarDirector(){
    if (this.CampusTipo == "Santiago") {
      return true;
    }
    else {
      return false;
    }
  }

  validadPredicador(){
    if (this.EventoTipo == "Oración" || this.EventoTipo == "Grupo Pequeño") {
      return false;
    } else{
      return true;
    }
  }

  validarMensaje(){
    if (this.EventoTipo == "Oración" || this.EventoTipo == "Presentación de Bebes" ||
      this.EventoTipo == "Bautismos" || this.EventoTipo == "Grupo Pequeño") {
      return false;
    } else {
      return true;
    }
  }

  // administradores = [
  //   { "name": "Rodrigo Quiroz & Priscilla Fabio" },
  //   { "name": "Miguel Gonzalez & Angelica Valencia" },
  //   { "name": "Philip Vera & Maria Jose Aguirre" },
  //   { "name": "Patricio Andres Burgos & Evelyn Fuentes" },
  //   { "name": "Pablo Encina & Naty Segura" },
  //   { "name": "Darwin Vargas & Jenny Larreal" },
  //   { "name": "N/A" }
  // ];

  // lideresVoluntario = [
  //   { "name": "Carlos Villalobos & Adry Piña" },
  //   { "name": "Cesar Letelier & Ghislaine Rivera" },
  //   { "name": "Jorge Briceño & luclenys Silva" },
  //   { "name": "Miguel Castro & Paulina Muñoz" },
  //   { "name": "Rodrigo Pozo & Kathy" },
  //   { "name": "Kevin Tapia & Euge Gonzalez" },
  //   { "name": "Josue Ferrer & Rocio Tiznado" },
  //   { "name": "N/A" }
  // ];

  // directores = [
  //   { "name": "Javiera Aguirre" },
  //   { "name": "Miguel Gonzalez & Angelica Valencia" },
  //   { "name": "N/A" }
  // ];

}
