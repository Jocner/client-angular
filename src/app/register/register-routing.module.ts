import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResgisterHomeComponent } from './resgister-home/resgister-home.component';

const routes: Routes = [
  { path: '', component: ResgisterHomeComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
