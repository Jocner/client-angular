import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterRoutingModule } from './register-routing.module';

import { MaterialModule } from '../material/material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EventComponent } from './event/event.component';
import { ResgisterHomeComponent } from './resgister-home/resgister-home.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { FollowerComponent } from './follower/follower.component';
import { ResponsibleComponent } from './responsible/responsible.component';




@NgModule({
  declarations: [
      EventComponent,
      ResgisterHomeComponent,
      AsistenciaComponent,
      FollowerComponent,
      ResponsibleComponent,
    ],
    imports: [
      NgbModule,
      MaterialModule,
      FormsModule,
      ReactiveFormsModule,
      RegisterRoutingModule,
      CommonModule
    ]
})
export class RegisterModule { }
