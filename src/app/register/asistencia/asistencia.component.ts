import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.css']
})
export class AsistenciaComponent {
  @Input() asistenciaForm!: FormGroup;
  @Input() nuevoAsistenciaGroup!: FormGroup;

  constructor() { }

  validadTipoEvento(){

  }
}
