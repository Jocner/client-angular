import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CampusResponse, Evento, ListaHorario } from 'src/app/interfaces/listas-response';
import { ListasService } from 'src/app/service/listas.service';


@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  @Input() dataEventForm!: FormGroup;
  @Input() nuevoEventGroup!: FormGroup;

  @Output() envioHorario: EventEmitter<string> = new EventEmitter();
  @Output() envioDescripcion: EventEmitter<string> = new EventEmitter();
  @Output() envioTipoEvento: EventEmitter<string> = new EventEmitter();
  @Output() envioTipoCampus: EventEmitter<string> = new EventEmitter();


  descripcion: string ="";
  horario: string ="";
  eventoTipo: string ="";
  tipoEvento: Evento[]=[];
  listaHorario: ListaHorario[] = [];
  listaCampus: CampusResponse[] = [];

  constructor(private listasService: ListasService) { }

  ngOnInit(): void {
    this.listasService.getTipoEvento().subscribe(response => {
      this.tipoEvento = response[0].evento;
      console.log(response);
    });
    this.listasService.getHorario().subscribe(response=>{
      this.listaHorario = response[0].listaHorario;
      console.log(response);
    });
    this.listasService.getCampus().subscribe(response=>{
      this.listaCampus = response;
      console.log(response);
    })

  }


  valorJunta(){
    if (this.dataEventForm.value.tipoEvento == "Junta"){
      return true;
    } else {
      this.envioDescripcion.emit("");
      return false;
    }
  }

  valorHora(){
    if (this.dataEventForm.value.hora == "Otro") {
      return true;
    } else {
      return false;
    }
  }

  recepcionHorario(event:any) {
    this.horario = event.target.value;
    this.envioHorario.emit(event.target.value);

  }

  selectTipoEvent(tipoEvento: string){
    this.eventoTipo = tipoEvento;
    this.envioTipoEvento.emit(tipoEvento);

  }

  selectTipoCampus(campus: string){
    this.envioTipoCampus.emit(campus);
  }

  validarTipoEvento(){
  if (this.eventoTipo == "Grupo Pequeño") {
      return false;
    } else {
      return true;
    }
  }


  // tipoEvento = [
  //   { "evento": "Oración" },
  //   { "evento": "Encuentro" },
  //   { "evento": "Presentación de Bebes" },
  //   { "evento": "Bautismos" },
  //   { "evento": "Junta" },
  //   { "evento" : "Grupo Pequeño"}
  // ];

  // listaHorario = [
  //   { "hora": "06:00" },
  //   { "hora": "09:00" },
  //   { "hora": "10:30" },
  //   { "hora": "10:45" },
  //   { "hora": "12:30" },
  //   { "hora": "16:45" },
  //   { "hora": "19:30" },
  //   { "hora": "20:00" },
  //   { "hora" : "Otro" }
  // ]


  // listaCampus = [
  //   { "campus": "Santiago" },
  //   { "campus": "Puente Alto" },
  //   { "campus": "MonteVideo" },
  //   { "campus": "Virtual" },
  // ];



}
