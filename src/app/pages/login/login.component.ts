import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginRequest } from 'src/app/interfaces/login-request';
import { LoginService } from 'src/app/service/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loginForm: FormGroup;
  valido: boolean = false;
  @Output() onSesion: EventEmitter<string> = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private loginService: LoginService) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(event: Event) {
    event.preventDefault();

    console.log(this.loginForm.value);
    if (this.loginForm.valid) {

      let login: LoginRequest = {
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      }
      this.valido = false;
      try{
      this.loginService.postToken(login).subscribe(response => {
        if (response[0].msg == "Ingreso Exitoso"){
          Swal.fire({
            icon: "success",
            title: "Inicio de Sesión",
            text: "Bienvenido ",
          });
          sessionStorage.setItem("token", response[0].token );
          sessionStorage.setItem("user", this.loginForm.value.email);
          this.onSesion.emit(this.loginForm.value.email);
        } else if (response[0].msg == "Password invalid") {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: "Contraseña incorrecta"
          });
        } else if (response[0].msg == "mail invalid") {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: "Mail incorrecto"
          });
        }else {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: "Usuario o contraseña incorrectos"
          });
        }
      })
    }
    catch(err) {
      console.log(err);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "error"
        });
    }

    }
  }

}

