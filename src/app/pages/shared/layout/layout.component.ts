import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent  {
  menus  = [
    {
      linkTitle: 'Register',
      link: '/register/register',
      icon: 'edit_note',
      profile: 1
    },
    {
      linkTitle: 'Analytics',
      link: '/analytics',
      icon: 'analytics',
      profile: 2
    },
    {
      linkTitle: 'Settings',
      link: '/setting',
      icon: 'settings',
      profile: 2
    },
  ];

  constructor(private router: Router) { }

  cerrarSession(){
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

}
