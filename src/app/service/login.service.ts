import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env }  from '../../environments/environment';
import { LoginRequest } from '../interfaces/login-request';
import { LoginResponse } from '../interfaces/login-response';

const urlBase = `${env.login.protocolo}${env.login.host}`;

@Injectable({
  providedIn: 'root'
})

export class LoginService {


  constructor(private http : HttpClient) { }

  postToken(login: LoginRequest): Observable<LoginResponse[]>{
    let url = urlBase + '/authentication'
    return this.http.post<LoginResponse[]>(url, login );
  }
}
