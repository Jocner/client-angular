import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env } from '../../environments/environment';
import { AdministradorResponse, CampusResponse, DirectorResponse, HorariosResponse, LiderResponse, TipoEventoResponse } from '../interfaces/listas-response';

const urlBase = `${env.listas.protocolo}${env.listas.host}`;

@Injectable({
  providedIn: 'root'
})
export class ListasService {

  constructor(private http: HttpClient) { }

  getCampus(): Observable<CampusResponse[]>{
    let url = urlBase + '/campus'
    return this.http.get<CampusResponse[]>(url);
  }

  getTipoEvento(): Observable<TipoEventoResponse[]>{
    let url = urlBase + '/evento'
    return this.http.get<TipoEventoResponse[]>(url);
  }

  getHorario(): Observable<HorariosResponse[]> {
    let url = urlBase + '/horario'
    return this.http.get<HorariosResponse[]>(url);
  }

  getLider(): Observable<LiderResponse[]> {
    let url = urlBase + '/lider'
    return this.http.get<LiderResponse[]>(url);
  }

  getAdministrador(): Observable<AdministradorResponse[]> {
    let url = urlBase + '/administrador'
    return this.http.get<AdministradorResponse[]>(url);
  }

  getDirector(): Observable<DirectorResponse[]> {
    let url = urlBase + '/director'
    return this.http.get<DirectorResponse[]>(url);
  }

}
