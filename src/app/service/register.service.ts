import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env } from '../../environments/environment';
import { RegisterRequest } from '../interfaces/register-request';
import { RegisterResponse } from '../interfaces/register-response';

const urlBase = `${env.register.protocolo}${env.register.host}`;

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  postRegister(register: RegisterRequest): Observable<RegisterResponse>{
    console.log(register);
    let url = urlBase + '/follower/';
    return this.http.post<RegisterResponse>(url, register);
  }
}
