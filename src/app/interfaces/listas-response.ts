export interface LiderResponse {
  id: string;
  nombres: string;
}


export interface AdministradorResponse {
  id: string;
  nombres: string;
}

export interface DirectorResponse {
  id: string;
  nombres: string;
}

export interface CampusResponse {
  id: string;
  campus: string;
}

export interface TipoEventoResponse {
  id: string;
  evento: Evento[];
}

export interface Evento {
  evento: string;
}

export interface HorariosResponse {
  id: string;
  listaHorario: ListaHorario[];
}

export interface ListaHorario {
  hora: string;
}

