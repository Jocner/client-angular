import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ar-front-register';
  iniciado: boolean = false;

  constructor(private router: Router){

  }


  recibirSesion(email: string){
    sessionStorage.setItem("user", email);
    this.iniciado = true;
    this.router.navigate(['register']);
  }
}
