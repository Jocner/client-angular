export const environment = {
  production: true,

  login: {
    protocolo: 'https://',
    host: 'srv-token-e3zg7nh7fq-uc.a.run.app',
  },

  register: {
    protocolo: 'https://',
    host: 'srv-follower-e7dbmiseaa-uk.a.run.app',
  },
  listas: {
    protocolo: 'https://',
    host: 'srv-register-7aayfv7yjq-uc.a.run.app',
  }

//Servicios de GCP
//Register --> https://srv-follower-e7dbmiseaa-uk.a.run.app
//login --> https://srv-token-e3zg7nh7fq-uc.a.run.app
//listas --> https://srv-register-7aayfv7yjq-uc.a.run.app

};
