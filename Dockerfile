# FROM node:16 as build

# WORKDIR /app
# COPY package*json .
# RUN npm install --force
# COPY . .
# RUN npm run build

# FROM node:16 
# WORKDIR /app
# COPY package.json .
# RUN npm install --force
# COPY --from=build /app/dist ./dist
# CMD npm run start


# FROM node:16
 

# WORKDIR  /app

 

# COPY ./ /app/

# COPY package*json . 

# RUN npm install --force

 

# EXPOSE 32000

 

# ENTRYPOINT npm start


# FROM node:16-alpine as build-step

# RUN mkdir -p /app

# WORKDIR /app

# COPY package.json /app

# RUN npm install --force

# COPY . /app

# RUN npm run build --force

# #Segunda Etapa
# FROM nginx:1.17.1-alpine
# 	#Si estas utilizando otra aplicacion cambia PokeApp por el nombre de tu app
# COPY --from=build-step /app/dist/ar-front-register /usr/share/nginx/html
# EXPOSE 34000
# CMD ["nginx","-g","daemon off;"]



# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:14 as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install 
COPY . ./
RUN npm run build:production 
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15
COPY --from=build-stage /app/dist/ar-front-register /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend

